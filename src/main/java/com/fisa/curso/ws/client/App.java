package com.fisa.curso.ws.client;

import java.util.List;

import com.fisa.curso.ws.Persona;
import com.fisa.curso.ws.PersonasResource_Service;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		
		PersonasResource_Service service = new PersonasResource_Service();
		List<Persona> personas = service.getPersonasResourceBeanPort().findPersonas("11");
		
		for(Persona p : personas){
			System.out.println(p.getApellido()+" "+p.getNombre());
		}
		
	}
}
